*** Settings ***    # robocop: disable=missing-doc-suite
Resource        ../../resources/setup_teardown.resource
Resource        ../../resources/account_management.resource
Resource        ../../page_objects/product_page.resource
Resource        ../../resources/cart_management.resource
Resource        ../../resources/product_management.resource

Test Setup      Basic Setup
Test Teardown   Delete Customer


*** Variables ***
&{PRODUCT_BASIC}    Product=Illustration vectorielle Renard
...                 Number=1
&{PRODUCT_DIMENSION}    Product=Affiche encadrée The best is yet to come
...                     Number=1
...                     Dimension=40x60cm
&{PRODUCT_CUSTOMIZATION}    Product=Mug personnalisable
...                         Customization=Joyeux anniversaire


*** Test Cases ***
# robocop: disable=missing-doc-test-case
Add One Product To The Cart
    Create An Example Customer And Log In
    Navigate To Category    art
    Go To Product Detail Page    ${PRODUCT_DIMENSION}[Product]
    Add Product To Cart
    Verify That The Cart Contains    ${PRODUCT_DIMENSION}

Add Two Products To The Cart
    Create An Example Customer And Log In
    Navigate To Category    art
    Go To Product Detail Page    ${PRODUCT_DIMENSION}[Product]
    Add Product To Cart
    Navigate To Category    art
    Go To Product Detail Page    ${PRODUCT_BASIC}[Product]
    Add Product To Cart
    Verify That The Cart Contains    ${PRODUCT_DIMENSION}    ${PRODUCT_BASIC}

Cannot Add A Product To The Cart If The Quantity Exceeds Stock
    Complete Logout
    Navigate To Category    accessoires
    Go To Product Detail Page    Mug Today is a good day
    Update Quantity    1599
    Out Of Stock Message Should Be Displayed    Le stock est insuffisant.
    Add Button Should Be Disabled

Add One Product With A Customized Message To The Cart
    Complete Logout
    Navigate To Category    accessoires
    Go To Product Detail Page    ${PRODUCT_CUSTOMIZATION}[Product]
    Customize With Message    ${PRODUCT_CUSTOMIZATION}[Customization]
    Add Product To Cart
    Verify That The Cart Contains    ${PRODUCT_CUSTOMIZATION}

Cannot Write A Customized Message If The Message Is Too Long
    ${message}    Catenate    SEPARATOR=${SPACE}
    ...    Un très long message qui dépasse les 250 caractères maximum autorisés.\nUn très long message qui dépasse
    ...    les 250 caractères maximum autorisés.\nUn très long message qui dépasse les 250 caractères maximum
    ...    autorisés.\nUn très long message qui dépasse les 250 caractères maximum autorisés.
    ${cut_message}    Catenate    SEPARATOR=${SPACE}
    ...    Un très long message qui dépasse les 250 caractères maximum autorisés. Un très long message qui dépasse
    ...    les 250 caractères maximum autorisés. Un très long message qui dépasse les 250 caractères maximum autorisés.
    ...    Un très long message qui dépasse les

    Complete Log Out
    Navigate To Category    accessoires
    Go To Product Detail Page    Mug personnalisable
    Customize With Message    ${message}
    Customization Message Should Be    ${cut_message}
