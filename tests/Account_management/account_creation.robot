*** Settings ***    # robocop: disable=missing-doc-suite
Library          squash_tf.TFParamService
Resource         ../../resources/setup_teardown.resource
Resource         ../../resources/navigation.resource
Resource         ../../resources/account_management.resource

Test Setup       Basic Setup
Test Teardown    Delete Customer


*** Test Cases ***
# robocop: disable=missing-doc-test-case,too-many-calls-in-test-case,too-long-test-case
Create An Account
    [Documentation]    This test confirms that customer-provided information in the
    ...                account creation form is successfully registered by verifying
    ...                that the customer can log in afterwards and her/his information
    ...                is accurately stored
    ${gender} =    Get Test Param    DS_gender
    ${first} =    Get Test Param    DS_first
    ${last} =    Get Test Param    DS_last
    ${password} =    Get Test Param    DS_password
    ${mail} =    Get Test Param    DS_mail
    ${birth} =    Get Test Param    DS_birth
    ${offers} =    Get Test Param    DS_offers
    ${news} =    Get Test Param    DS_news

    &{customer} =    Create Dictionary    gender=${gender}    first_name=${first}    last_name=${last}
    ...                                   email=${mail}    password=${password}    birth_date=${birth}
    ...                                   offers=${offers}    privacy=yes    newsletter=${news}    gdpr=yes

    Go To The AccountCreation Page
    Fill AccountCreation Fields And Store Credentials    &{customer}
    Complete Logout
    Log In With An Email And Password    ${mail}    ${password}
    Go To The MyIdentity Page

    &{created_customer_information} =    Create Dictionary    gender=${gender}    first_name=${first}
    ...                                   last_name=${last}    email=${mail}    password=${password}
    ...                                   birth_date=${birth}    offers=${offers}    privacy=no
    ...                                   newsletter=${news}    gdpr=no

    Verify Personal Information    &{created_customer_information}

Cannot Create An Account When The Email Format Is Invalid
    ${gender} =    Get Test Param    DS_gender
    ${first} =    Get Test Param    DS_first
    ${last} =    Get Test Param    DS_last
    ${password} =    Get Test Param    DS_password
    ${mail} =    Get Test Param    DS_mail
    ${birth} =    Get Test Param    DS_birth
    ${offers} =    Get Test Param    DS_offers
    ${privacy} =    Get Test Param    DS_privacy
    ${news} =    Get Test Param    DS_news
    ${gdpr} =    Get Test Param    DS_gdpr
    ${field} =    Get Test Param    DS_field

    &{customer} =    Create Dictionary    gender=${gender}    first_name=${first}    last_name=${last}
    ...                                   email=${mail}    password=${password}    birth_date=${birth}
    ...                                   offers=${offers}    privacy=${privacy}    newsletter=${news}
    ...                                   gdpr=${gdpr}

    Go To The AccountCreation Page
    Fill AccountCreation Fields And Store Credentials    &{customer}
    Page Should Be    AccountCreation
    Element Should Have A Validation Message Property   ${field}

Cannot Create An Account When The Password Is Too Short
    &{customer} =    Create Dictionary    gender=F    first_name=Niloofar    last_name=Norooz
    ...                                   email=niloofar@mail.com    password=word    birth_date=01/08/1993
    ...                                   offers=yes    privacy=yes    newsletter=yes    gdpr=yes
    Go To The AccountCreation Page
    Fill AccountCreation Fields And Store Credentials    &{customer}
    Page Should Be    AccountCreation
    Element Should Have A Validation Message Property    password_txt

Cannot Create An Account When Privacy And/Or GDPR Are Not Checked
    ${gender} =    Get Test Param    DS_gender
    ${first} =    Get Test Param    DS_first
    ${last} =    Get Test Param    DS_last
    ${password} =    Get Test Param    DS_password
    ${mail} =    Get Test Param    DS_mail
    ${birth} =    Get Test Param    DS_birth
    ${offers} =    Get Test Param    DS_offers
    ${privacy} =    Get Test Param    DS_privacy
    ${news} =    Get Test Param    DS_news
    ${gdpr} =    Get Test Param    DS_gdpr
    ${field} =    Get Test Param    DS_field

    &{customer} =    Create Dictionary    gender=${gender}    first_name=${first}    last_name=${last}
    ...                                   email=${mail}    password=${password}    birth_date=${birth}
    ...                                   offers=${offers}    privacy=${privacy}
    ...                                   newsletter=${news}    gdpr=${gdpr}

    Go To The AccountCreation Page
    Fill AccountCreation Fields And Store Credentials    &{customer}
    Page Should Be    AccountCreation
    Element Should Have A Validation Message Property    ${field}
