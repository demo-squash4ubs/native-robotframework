*** Settings ***    # robocop: disable=missing-doc-suite
Library           squash_tf.TFParamService
Resource          ../../resources/setup_teardown.resource
Resource          ../../resources/account_management.resource
Resource          ../../resources/navigation.resource

Test Setup        Basic Setup
Test Teardown     Delete Customer


*** Variables ***
&{BASE_CUSTOMER}    gender=M    first_name=john    last_name=doe    password=pass1234
...        email=johndoe@mail.com    birth_date=01/01/1950    offers=yes    newsletter=yes


*** Test Cases ***
# robocop: disable=missing-doc-test-case,too-many-calls-in-test-case,too-long-test-case
Modify The Account Information
    ${gender} =    Get Test Param    DS_gender
    ${first} =    Get Test Param    DS_first
    ${last} =    Get Test Param    DS_last
    ${password} =    Get Test Param    DS_password
    ${mail} =    Get Test Param    DS_mail
    ${birth} =    Get Test Param    DS_birth
    ${offers} =    Get Test Param    DS_offers
    ${news} =    Get Test Param    DS_news

    &{customer_new_info} =    Create Dictionary    gender=${gender}    first_name=${first}    last_name=${last}
    ...                                   email=${mail}    old_password=${BASE_CUSTOMER}[password]
    ...                                   new_password=${password}    birth_date=${birth}    offers=${offers}
    ...                                   privacy=yes    newsletter=${news}    gdpr=yes

    Create A Customer And Log In    &{BASE_CUSTOMER}
    Fill MyIdentity Fields    &{customer_new_info}

    &{modified_customer_info} =    Create Dictionary    gender=${gender}    first_name=${first}    last_name=${last}
    ...                                   email=${mail}    new_password=${password}    birth_date=${birth}
    ...                                   offers=${offers}    privacy=no    newsletter=${news}    gdpr=no
    Message Should Be Displayed    Information mise à jour avec succès.
    Verify Personal Information    &{modified_customer_info}

Cannot Modify The Account Information When Privacy And/Or GDPR Are Not Checked
    ${gender} =    Get Test Param    DS_gender
    ${first} =    Get Test Param    DS_first
    ${last} =    Get Test Param    DS_last
    ${password} =    Get Test Param    DS_password
    ${mail} =    Get Test Param    DS_mail
    ${birth} =    Get Test Param    DS_birth
    ${offers} =    Get Test Param    DS_offers
    ${news} =    Get Test Param    DS_news
    ${field} =    Get Test Param    DS_field

    &{customer_new_info} =    Create Dictionary    gender=${gender}    first_name=${first}    last_name=${last}
    ...                                   email=${mail}    old_password=${BASE_CUSTOMER}[password]
    ...                                   new_password=${password}    birth_date=${birth}    offers=${offers}
    ...                                   privacy=no    newsletter=${news}    gdpr=no

    Create A Customer And Log In    &{BASE_CUSTOMER}
    Fill MyIdentity Fields    &{customer_new_info}
    Page Should Be    MyIdentity
    Element Should Have A Validation Message Property    ${field}
