*** Settings ***    # robocop: disable=missing-doc-suite
Library          squash_tf.TFParamService
Resource         ../../resources/setup_teardown.resource
Resource         ../../resources/account_management.resource
Resource         ../../resources/cart_management.resource
Resource         ../../resources/order_management.resource

Test Setup       Run Keywords    Basic Setup
Test Teardown    Run Keywords    Delete Order    Delete Customer


*** Variables ***
&{PRODUCT_MUG_3}    Product=Mug The best is yet to come
...             Number=3
...             UnitPrice=14,28
&{PRODUCT_MUG_1}    Product=Mug The best is yet to come
...             Number=1
...             UnitPrice=14,28
&{PRODUCT_ILLUSTRATION}    Product=Illustration vectorielle Renard
...             Number=2
...             UnitPrice=10,80
@{PRODUCTS}         ${PRODUCT_MUG_3}    ${PRODUCT_ILLUSTRATION}

&{BASE_CUSTOMER}    gender=M    first_name=john    last_name=doe    password=pass1234
...        email=johndoe@mail.com    birth_date=01/01/1950    offers=yes    newsletter=yes


*** Test Cases ***
# robocop: disable=missing-doc-test-case,too-many-calls-in-test-case,too-long-test-case
Place An Order - Logged-in Customer
    ${alias} =    Get Test Param    DS_alias
    ${company} =    Get Test Param    DS_company
    ${vat} =    Get Test Param    DS_vat
    ${address} =    Get Test Param    DS_address
    ${supp} =    Get Test Param    DS_supp
    ${zip} =    Get Test Param    DS_zip
    ${city} =    Get Test Param    DS_city
    ${country} =    Get Test Param    DS_country
    ${phone} =    Get Test Param    DS_phone
    ${delivery} =    Get Test Param    DS_delivery
    ${message} =    Get Test Param    DS_message
    ${paymode} =    Get Test Param    DS_paymode
    ${total_price} =    Get Test Param    DS_total_price

    &{address_information} =    Create Dictionary    alias=${alias}    company=${company}
    ...                       vat=${vat}    address=${address}    address_supp=${supp}
    ...                       zip=${zip}    city=${city}    country=${country}
    ...                       phone=${phone}    facturation=yes

    Create A Customer And Log In    &{BASE_CUSTOMER}
    Set Initial Cart State    @{PRODUCTS}
    Initiate Order Placement Process
    Fill And Submit The Address Form    ADRESSES    &{address_information}
    Fill And Submit Delivery Form    MODE DE LIVRAISON    ${delivery}    ${message}
    Fill And Submit Payment Form    PAIEMENT    ${paymode}    yes
    Verify That The Order Is Placed And Contains    @{PRODUCTS}
    Verify That The Total Order Price Is "${total_price}"

Place An Order - Logged-out Customer
    ${alias} =    Get Test Param    DS_alias
    ${company} =    Get Test Param    DS_company
    ${vat} =    Get Test Param    DS_vat
    ${address} =    Get Test Param    DS_address
    ${supp} =    Get Test Param    DS_supp
    ${zip} =    Get Test Param    DS_zip
    ${city} =    Get Test Param    DS_city
    ${country} =    Get Test Param    DS_country
    ${phone} =    Get Test Param    DS_phone
    ${delivery} =    Get Test Param    DS_delivery
    ${message} =    Get Test Param    DS_message
    ${paymode} =    Get Test Param    DS_paymode
    ${total_price} =    Get Test Param    DS_total_price

    &{address_information} =    Create Dictionary    alias=${alias}    company=${company}
    ...                       vat=${vat}    address=${address}    address_supp=${supp}
    ...                       zip=${zip}    city=${city}    country=${country}
    ...                       phone=${phone}    facturation=yes

    Create A Customer And Log Out    &{BASE_CUSTOMER}
    Customer Should Be Logged Out
    Navigate To Category    accessoires
    Go To Product Detail Page    ${PRODUCT_MUG_1}[Product]
    Add Product To Cart
    Initiate Order Placement Process
    Fill And Submit The Login Form    INFORMATIONS PERSONNELLES    ${BASE_CUSTOMER}[email]   ${BASE_CUSTOMER}[password]
    Fill And Submit The Address Form    ADRESSES    &{address_information}
    Fill And Submit Delivery Form    MODE DE LIVRAISON    ${delivery}    ${message}
    Fill And Submit Payment Form    PAIEMENT    ${paymode}    yes
    Verify That The Order Is Placed And Contains    ${PRODUCT_MUG_1}
    Verify That The Total Order Price Is "${total_price}"

Cannot Place An Order If The Sale Conditions Are Not Approved
    Create A Customer And Log In    &{BASE_CUSTOMER}
    Set Initial Cart State    @{PRODUCTS}
    Initiate Order Placement Process

    &{address_information} =    Create Dictionary    alias=add1   company=${EMPTY}
    ...                       vat=${EMPTY}    address=1 rue du chat    address_supp=${EMPTY}
    ...                       zip=12345    city=Paris    country=France
    ...                       phone=${EMPTY}    facturation=yes

    Fill And Submit The Address Form    ADRESSES    &{address_information}
    Fill And Submit Delivery Form    MODE DE LIVRAISON    prestashop    ${EMPTY}
    Verify Current Section    PAIEMENT
    Fill Payment Form    virement bancaire    no
    Page Should Be    Order
    Step Should Be Filling Payment
    Submit Button Should Be Disabled
